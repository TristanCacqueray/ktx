# KTX

> KTX is a format for storing textures for OpenGL® and OpenGL® ES applications.
> It is distinguished by the simplicity of the loader required to instantiate a
> GL texture object from the file contents.

- KTX 1: https://registry.khronos.org/KTX/specs/1.0/ktxspec.v1.html
- KTX 2: https://registry.khronos.org/KTX/specs/2.0/ktxspec.v2.html#_abstract

## Packages

* [ktx-codec] - Raw format codec for KTX data. Actual texture loading is delegated to a rendering engine.

That's it, for now.

[ktx-codec]: https://hackage.haskell.org/package/ktx-codec
